# Mortgage Project for ING

This application implements backend of Mortgage.

## Technical Stack

    * Java 19
    * Springboot 3.0.2
    * Maven 3.6.3
    * Lombok
	* ModelMapper for object mapping
    * Swagger 2 API documentation
    * Junit for TDD
	* Single Responsibilities
	* Rest Service Standarts
	* Singleton Pattern

# Getting Started

## Installation

1. Clone the repo

     https://gitlab.com/Aadiguzel/mortgage
2. Import project in IntellJ IDEA

3. Choose the Spring Boot Application file (search for @SpringBootApplication)

4. Right Click on the file and Run as Java Application

## Building from source and Run Project

    mvn clean install
    java -jar target/mortgage-0.0.1-SNAPSHOT.jar


## Usage

There are 2 API's in this Project.

* `/api/interest-rates`
    Imports a list of interest rates depend on maturity periods.
* `/api/mortgage-check`
    Checks a given mortgage request is available for mortgage usage, if it is feasible then returns the monthly cost.
    The request has 
  - Home Value
  - Loan Value
  - Income
  - Maturity Period

## Test on the browser via SWAGGER

http://localhost:8080/swagger-ui.html#/mancala

## Author
- Alparslan Adiguzel