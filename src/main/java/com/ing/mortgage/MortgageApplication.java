package com.ing.mortgage;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan({"com.ing.mortgage.controller","com.ing.mortgage.service"})
public class MortgageApplication {
	public static void main(String[] args) {
		SpringApplication.run(MortgageApplication.class, args);
	}
}