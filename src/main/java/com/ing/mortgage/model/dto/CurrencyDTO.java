package com.ing.mortgage.model.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyDTO {
    private long code;
    private String name;
    private String symbol;
}
