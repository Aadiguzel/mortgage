package com.ing.mortgage.model.dto;

import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AmountDTO {
    Double value;
    CurrencyDTO currency;
}
