package com.ing.mortgage.model.dto.request;

import com.ing.mortgage.model.dto.AmountDTO;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckMortgageRequestDTO {
    private AmountDTO income;
    private int maturityPeriod;
    private AmountDTO loanValue;
    private AmountDTO homeValue;
}
