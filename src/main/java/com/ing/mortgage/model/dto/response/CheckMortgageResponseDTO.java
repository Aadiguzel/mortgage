package com.ing.mortgage.model.dto.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckMortgageResponseDTO {
    private boolean isFeasible;
    private double monthlyCost;
}
