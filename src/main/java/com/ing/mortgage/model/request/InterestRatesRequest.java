package com.ing.mortgage.model.request;

import com.ing.mortgage.model.entity.MortgageInterestEntity;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InterestRatesRequest {
    private List<MortgageInterestEntity> mortgageInterestList;
}
