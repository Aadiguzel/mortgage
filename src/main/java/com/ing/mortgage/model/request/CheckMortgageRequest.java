package com.ing.mortgage.model.request;

import com.ing.mortgage.controller.validator.annotation.MortgageValidator;
import com.ing.mortgage.model.dto.AmountDTO;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MortgageValidator
public class CheckMortgageRequest {
    private AmountDTO income;
    private int maturityPeriod;
    private AmountDTO loanValue;
    private AmountDTO homeValue;
}
