package com.ing.mortgage.model.response;

import com.ing.mortgage.model.dto.AmountDTO;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckMortgageResponse {
    private boolean isFeasible;
    private double monthlyCost;
}
