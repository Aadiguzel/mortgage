package com.ing.mortgage.model.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends RuntimeException {
    private  String errMsgKey;
    private  String errorCode;

    public BusinessException(ErrorCode code) {
        super(code.getErrMsgKey());
        this.errMsgKey = code.getErrMsgKey();
        this.errorCode = code.getErrCode();
    }

    public BusinessException(String message) {
        super(message);
    }
}

