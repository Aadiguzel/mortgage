package com.ing.mortgage.model.exceptions;

import lombok.Getter;

@Getter
public enum ErrorCode {

    HOME_VALUE_EXCEPTION("ERR-001", "Home Value is equal or less then zero"),
    LOAN_VALUE_EXCEPTION("ERR-002", "Loan Value is equal or less then zero"),
    INCOME_EXCEPTION("ERR-003", "Income is equal or less then zero"),
    MATURITY_PERIOD_EXCEPTION("ERR-004", "Maturity Period is equal or less then zero"),
    INTEREST_RATE_LIST_EXCEPTION("ERR-005", "Interest Rate list is empty"),
    INTEREST_RATE_EXCEPTION("ERR-006", "Interest Rate is equal or less then zero"),
    LAST_UPDATE_EXCEPTION("ERR-007", "Last Update is not available date"),
    INTEREST_RATE_NOT_FOUND_EXCEPTION("ERR-008", "Interest Rate not found in list");
    private String errCode;
    private String errMsgKey;

    private ErrorCode(final String errCode, final String errMsgKey) {
        this.errCode = errCode;
        this.errMsgKey = errMsgKey;
    }
}
