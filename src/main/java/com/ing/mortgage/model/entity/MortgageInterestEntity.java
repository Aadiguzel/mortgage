package com.ing.mortgage.model.entity;

import com.ing.mortgage.controller.validator.annotation.InterestRateValidator;
import lombok.*;
import org.springframework.cache.annotation.CacheConfig;

import java.time.OffsetDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@InterestRateValidator
public class MortgageInterestEntity {
    private int maturityPeriod;
    private float interestRate;
    private OffsetDateTime lastUpdate;
}
