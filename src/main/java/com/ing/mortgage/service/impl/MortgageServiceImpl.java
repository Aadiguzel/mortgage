package com.ing.mortgage.service.impl;

import com.ing.mortgage.model.dto.request.CheckMortgageRequestDTO;
import com.ing.mortgage.model.dto.response.CheckMortgageResponseDTO;
import com.ing.mortgage.model.entity.MortgageInterestEntity;
import com.ing.mortgage.model.exceptions.BusinessException;
import com.ing.mortgage.model.exceptions.ErrorCode;
import com.ing.mortgage.service.MortgageService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class MortgageServiceImpl implements MortgageService {

    private  List<MortgageInterestEntity> InterestRateList;

    public MortgageServiceImpl(List<MortgageInterestEntity> request) {
        this.InterestRateList = request;
    }

    public synchronized void setMortgageInterestRates(List<MortgageInterestEntity> request){
        if(!Objects.isNull(this.InterestRateList))
            this.InterestRateList.clear();
        else
            this.InterestRateList = new ArrayList<>();

        this.InterestRateList.addAll(request);
    }

    public synchronized Optional<MortgageInterestEntity> getMortgageInterestRatesByMaturityPeriod(int maturityPeriod){
        return this.InterestRateList.stream().filter(x -> x.getMaturityPeriod() == maturityPeriod).toList().stream().findFirst();
    }

    public CheckMortgageResponseDTO checkMortgage(CheckMortgageRequestDTO request){
        return CheckMortgageResponseDTO.builder().
                isFeasible(isFeasible(request)).
                monthlyCost(calculateMonthlyCost(request)).build();
    }

    private boolean isFeasible(CheckMortgageRequestDTO request){
        return checkIncome(request) && checkHomeValue(request);
    }

    private boolean checkIncome(CheckMortgageRequestDTO request){
        return Double.compare((request.getIncome().getValue() * 4) ,request.getLoanValue().getValue()) >= 0;
    }

    private boolean checkHomeValue(CheckMortgageRequestDTO request){
        return request.getHomeValue().getValue().compareTo(request.getLoanValue().getValue()) >= 0;
    }

    private double calculateMonthlyCost(CheckMortgageRequestDTO request){
        Optional<MortgageInterestEntity> mortgageInterestRates = this.getMortgageInterestRatesByMaturityPeriod(request.getMaturityPeriod());
        if(mortgageInterestRates.isPresent()) {
            float rate = (mortgageInterestRates.get().getInterestRate() / 100) / 12;
            int time = request.getMaturityPeriod() * 12;
            double principal = request.getLoanValue().getValue();

            double q = Math.pow(1 + rate, -time);
            return (principal * rate) * 1000 / (1 - q);
        }else{
            throw new BusinessException(ErrorCode.INTEREST_RATE_NOT_FOUND_EXCEPTION);
        }
    }
}
