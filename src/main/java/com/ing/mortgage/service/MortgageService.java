package com.ing.mortgage.service;

import com.ing.mortgage.model.dto.request.CheckMortgageRequestDTO;
import com.ing.mortgage.model.dto.response.CheckMortgageResponseDTO;
import com.ing.mortgage.model.entity.MortgageInterestEntity;

import java.util.List;
import java.util.Optional;

public interface MortgageService {
    void setMortgageInterestRates(List<MortgageInterestEntity> request);

    Optional<MortgageInterestEntity> getMortgageInterestRatesByMaturityPeriod(int maturityPeriod);

    CheckMortgageResponseDTO checkMortgage(CheckMortgageRequestDTO request);

}