package com.ing.mortgage.controller.validator;

import com.ing.mortgage.controller.validator.annotation.InterestRateValidator;
import com.ing.mortgage.model.exceptions.BusinessException;
import com.ing.mortgage.model.exceptions.ErrorCode;
import com.ing.mortgage.model.request.InterestRatesRequest;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.OffsetDateTime;
import java.util.Objects;


@Component
public class InterestRatesRequestValidator implements ConstraintValidator<InterestRateValidator, InterestRatesRequest> {
    @Override
    public void initialize(InterestRateValidator constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(InterestRatesRequest interestRatesRequest, ConstraintValidatorContext constraintValidatorContext) {
        if (Objects.isNull(interestRatesRequest) || Objects.isNull(interestRatesRequest.getMortgageInterestList()) || interestRatesRequest.getMortgageInterestList().isEmpty()) {
            throw new BusinessException(ErrorCode.INTEREST_RATE_LIST_EXCEPTION);
        }

        if (!interestRatesRequest.getMortgageInterestList().stream().filter(x->x.getMaturityPeriod() <= 0).toList().isEmpty()) {
            throw new BusinessException(ErrorCode.MATURITY_PERIOD_EXCEPTION);
        }

        if (!interestRatesRequest.getMortgageInterestList().stream().filter(x->x.getLastUpdate().isEqual(OffsetDateTime.MIN)).toList().isEmpty()) {
            throw new BusinessException(ErrorCode.LAST_UPDATE_EXCEPTION);
        }
        return true;
    }

}