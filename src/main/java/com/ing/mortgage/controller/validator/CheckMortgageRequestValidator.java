package com.ing.mortgage.controller.validator;

import com.ing.mortgage.controller.validator.annotation.MortgageValidator;
import com.ing.mortgage.model.constant.Constants;
import com.ing.mortgage.model.exceptions.BusinessException;
import com.ing.mortgage.model.exceptions.ErrorCode;
import com.ing.mortgage.model.request.CheckMortgageRequest;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;


@Component
public class CheckMortgageRequestValidator implements ConstraintValidator<MortgageValidator, CheckMortgageRequest> {
    @Override
    public void initialize(MortgageValidator constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(CheckMortgageRequest checkMortgageRequest, ConstraintValidatorContext constraintValidatorContext) {
        if (Objects.isNull(checkMortgageRequest.getHomeValue()) || checkMortgageRequest.getHomeValue().getValue() <= Constants.NUMBER_ZERO) {
            throw new BusinessException(ErrorCode.HOME_VALUE_EXCEPTION);
        }

        if (Objects.isNull(checkMortgageRequest.getLoanValue()) || checkMortgageRequest.getLoanValue().getValue() <= Constants.NUMBER_ZERO) {
            throw new BusinessException(ErrorCode.LOAN_VALUE_EXCEPTION);
        }

        if (Objects.isNull(checkMortgageRequest.getIncome()) || checkMortgageRequest.getIncome().getValue() <= Constants.NUMBER_ZERO) {
            throw new BusinessException(ErrorCode.INCOME_EXCEPTION);
        }

        if (checkMortgageRequest.getMaturityPeriod() <= Constants.NUMBER_ZERO) {
            throw new BusinessException(ErrorCode.MATURITY_PERIOD_EXCEPTION.getErrMsgKey());
        }
        return true;
    }
}