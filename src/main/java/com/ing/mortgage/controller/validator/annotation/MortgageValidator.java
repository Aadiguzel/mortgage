package com.ing.mortgage.controller.validator.annotation;

import com.ing.mortgage.controller.validator.CheckMortgageRequestValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = CheckMortgageRequestValidator.class)
@Documented
public @interface MortgageValidator {
    String message() default "{MortgageValidator.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}