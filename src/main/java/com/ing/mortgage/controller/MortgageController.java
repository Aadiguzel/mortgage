package com.ing.mortgage.controller;


import com.ing.mortgage.model.dto.request.CheckMortgageRequestDTO;
import com.ing.mortgage.model.dto.response.CheckMortgageResponseDTO;
import com.ing.mortgage.model.request.CheckMortgageRequest;
import com.ing.mortgage.model.request.InterestRatesRequest;
import com.ing.mortgage.model.response.CheckMortgageResponse;
import com.ing.mortgage.service.MortgageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping({"/api"})
public class MortgageController {

    @Autowired
    private MortgageService mortgageService;

    @GetMapping("/interest-rates")
    public void setInterestRates(@RequestBody @Valid InterestRatesRequest request) {
        mortgageService.setMortgageInterestRates(request.getMortgageInterestList());
    }

    @PostMapping("/mortgage-check")
    public CheckMortgageResponse checkMortgage(@RequestBody @Valid CheckMortgageRequest request) {
        CheckMortgageRequestDTO requestDTO = CheckMortgageRequestDTO.builder().income(request.getIncome())
                                                                                .homeValue(request.getHomeValue())
                                                                                .maturityPeriod(request.getMaturityPeriod())
                                                                                .loanValue(request.getLoanValue()).build();

        CheckMortgageResponseDTO responseDTO = mortgageService.checkMortgage(requestDTO);
        return CheckMortgageResponse.builder().isFeasible(responseDTO.isFeasible()).monthlyCost(responseDTO.getMonthlyCost()).build();
    }

}
