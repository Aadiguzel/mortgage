package com.ing.mortgage.controller.validator;

import com.ing.mortgage.model.dto.AmountDTO;
import com.ing.mortgage.model.dto.CurrencyDTO;
import com.ing.mortgage.model.request.CheckMortgageRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CheckMortgageRequestValidatorTests {
    @InjectMocks
    CheckMortgageRequestValidator validator;
    @Mock
    ConstraintValidatorContext context;

    @Test
    public void should_HomeValueGreaterThanZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest((double) 0, 1.0, 2.0,3), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_LoanValueGreaterThanZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(1.0, (double) 0,  2.0,3), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_IncomeValueGreaterThanZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(1.0, 2.0, (double) 0,3), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_maturityPeriodValueGreaterThanZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(1.0, 2.0, 3.0,0), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    CheckMortgageRequest createRequest(Double homeVal, Double loanVal, Double incomeVal, int maturityPeriod) {
        return CheckMortgageRequest.builder().homeValue(createAmount(homeVal))
                                             .loanValue(createAmount(loanVal))
                                             .income(createAmount(incomeVal))
                                             .maturityPeriod(maturityPeriod).build();
    }

    private AmountDTO createAmount(Double val) {
        return AmountDTO.builder().value(val).currency(createCurrency()).build();
    }

    private CurrencyDTO createCurrency() {
        return CurrencyDTO.builder().symbol("€")
                                    .code(0)
                                    .name("Euro").build();
    }


}
