package com.ing.mortgage.controller.validator;

import com.ing.mortgage.model.dto.AmountDTO;
import com.ing.mortgage.model.dto.CurrencyDTO;
import com.ing.mortgage.model.entity.MortgageInterestEntity;
import com.ing.mortgage.model.request.CheckMortgageRequest;
import com.ing.mortgage.model.request.InterestRatesRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class InterestRatesRequestValidatorTests {
    @InjectMocks
    InterestRatesRequestValidator validator;
    @Mock
    ConstraintValidatorContext context;

    @Test
    public void should_mortgageInterestListExists() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(false,0, 1,1,OffsetDateTime.now()), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_mortgageInterestListCountGreaterThenZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(true,0, 1,1,OffsetDateTime.now()), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_maturityPeriodGreaterThenZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(true,1,0,1,OffsetDateTime.now()), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_InterestRateGreaterThenZero() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(true,1,1,0,OffsetDateTime.now()), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    @Test
    public void should_UpdateTimeAvailable() {
        boolean isValid = false;
        try {
            isValid = validator.isValid(createRequest(true,1,1,1,OffsetDateTime.MIN), context);
            assertTrue(isValid);
        }catch (Exception e){
            assertFalse(isValid);
        }
    }

    InterestRatesRequest createRequest(boolean listExists, int listCount, int maturityPeriod, float interestRate, OffsetDateTime lastUpdate) {
        if(listExists)
            return InterestRatesRequest.builder().mortgageInterestList(createMortgageInterestList(listCount,maturityPeriod, interestRate, lastUpdate))
                                                 .build();
        else
            return InterestRatesRequest.builder().build();
    }

    private List<MortgageInterestEntity> createMortgageInterestList(int listCount, int maturityPeriod, float interestRate, OffsetDateTime lastUpdate) {

        if (listCount == 0)
            return new ArrayList<>();
        else{
            List<MortgageInterestEntity> mortgageInterestEntities = new ArrayList<>();
            int counter = 1;
            while(counter <= listCount){
                mortgageInterestEntities.add(createMortgageInterest(maturityPeriod,interestRate,lastUpdate));
                counter++;
            }

            return mortgageInterestEntities;
        }
    }

    private MortgageInterestEntity createMortgageInterest(int maturityPeriod, float interestRate, OffsetDateTime lastUpdate) {
        return MortgageInterestEntity.builder().maturityPeriod(maturityPeriod)
                                                .interestRate(interestRate)
                                                .lastUpdate(lastUpdate)
                                                .build();
    }

}
