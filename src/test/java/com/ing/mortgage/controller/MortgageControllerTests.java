package com.ing.mortgage.controller;

import com.ing.mortgage.model.dto.AmountDTO;
import com.ing.mortgage.model.dto.CurrencyDTO;
import com.ing.mortgage.model.dto.response.CheckMortgageResponseDTO;
import com.ing.mortgage.model.entity.MortgageInterestEntity;
import com.ing.mortgage.model.request.CheckMortgageRequest;
import com.ing.mortgage.model.request.InterestRatesRequest;
import com.ing.mortgage.model.response.CheckMortgageResponse;
import com.ing.mortgage.service.MortgageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MortgageControllerTests {

    @InjectMocks
    MortgageController mortgageController;

    @Mock
    MortgageService mortgageService;

    @Test
    public void should_CreateInterestRatesList() {
        mortgageController.setInterestRates(new InterestRatesRequest());
        assertTrue(Boolean.TRUE);
    }

    @Test
    public void should_CheckMortgage() {
        when(mortgageService.checkMortgage(any())).thenReturn(createCheckMortgageResponseDTO());
        CheckMortgageResponse response = mortgageController.checkMortgage(createCheckMortgageRequest(1.0,1.0,1.0,1));
        assertEquals(response.isFeasible(), Boolean.FALSE);
    }

    private CheckMortgageResponseDTO createCheckMortgageResponseDTO() {
        return CheckMortgageResponseDTO.builder().build();
    }

    private CheckMortgageRequest createCheckMortgageRequest(Double homeVal, Double loanVal, Double incomeVal, int maturityPeriod) {
        return CheckMortgageRequest.builder().homeValue(createAmount(homeVal))
                .loanValue(createAmount(loanVal))
                .income(createAmount(incomeVal))
                .maturityPeriod(maturityPeriod).build();
    }


    private AmountDTO createAmount(Double val) {
        return AmountDTO.builder().value(val).currency(createCurrency()).build();
    }

    private CurrencyDTO createCurrency() {
        return CurrencyDTO.builder().symbol("€")
                .code(0)
                .name("Euro").build();
    }

    private List<MortgageInterestEntity> createMortgageInterestList() {
        List<MortgageInterestEntity> mortgageInterestEntities = new ArrayList<>();
        mortgageInterestEntities.add(createMortgageInterest(1, 1, OffsetDateTime.now()));

        return mortgageInterestEntities;
    }

    private MortgageInterestEntity createMortgageInterest(int maturityPeriod, float interestRate, OffsetDateTime lastUpdate) {
        return MortgageInterestEntity.builder().maturityPeriod(maturityPeriod)
                .interestRate(interestRate)
                .lastUpdate(lastUpdate)
                .build();
    }
}