package com.ing.mortgage.service;

import com.ing.mortgage.model.dto.AmountDTO;
import com.ing.mortgage.model.dto.CurrencyDTO;
import com.ing.mortgage.model.dto.request.CheckMortgageRequestDTO;
import com.ing.mortgage.model.dto.response.CheckMortgageResponseDTO;
import com.ing.mortgage.model.entity.MortgageInterestEntity;
import com.ing.mortgage.model.request.InterestRatesRequest;
import com.ing.mortgage.service.impl.MortgageServiceImpl;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MortgageServiceTests {
    @InjectMocks
    MortgageServiceImpl service;

    @Test
    public void should_setMortgageInterestRates() {
        service.setMortgageInterestRates(createMortgageInterestEntityList());
        assertTrue(Boolean.TRUE);
    }

    @Test
    public void should_getMortgageInterestRatesByMaturityPeriod(){
        service.setMortgageInterestRates(createMortgageInterestEntityList());
        Optional<MortgageInterestEntity> response = service.getMortgageInterestRatesByMaturityPeriod(1);
        assertEquals(response.isEmpty(), Boolean.FALSE);
    }

    @Test
    public void should_checkMortgage() {
        service.setMortgageInterestRates(createMortgageInterestEntityList());
        CheckMortgageResponseDTO response = service.checkMortgage(createCheckMortgageRequestDTO());
        assertEquals(response.isFeasible(), Boolean.TRUE);
    }

    private CheckMortgageRequestDTO createCheckMortgageRequestDTO() {
        return CheckMortgageRequestDTO.builder().income(createAmount(10.0))
                                                .homeValue(createAmount(10.0))
                                                .loanValue(createAmount(10.0))
                                                .maturityPeriod(1).build();
    }

    private List<MortgageInterestEntity> createMortgageInterestEntityList() {
        List<MortgageInterestEntity> mortgageInterestEntities = new ArrayList<>();
        mortgageInterestEntities.add(createMortgageInterestEntity());
        return mortgageInterestEntities;
    }

    private MortgageInterestEntity createMortgageInterestEntity(){
        return MortgageInterestEntity.builder().interestRate(1)
                                                .maturityPeriod(1)
                                                .lastUpdate(OffsetDateTime.now()).build();
    }

    private AmountDTO createAmount(Double val) {
        return AmountDTO.builder().value(val).currency(createCurrency()).build();
    }

    private CurrencyDTO createCurrency() {
        return CurrencyDTO.builder().symbol("€")
                .code(0)
                .name("Euro").build();
    }
}
